import * as EventSource from 'eventsource';
import { enableAmbilight, disableAmbilight } from './ambilight';

export function establishOpenHabConnection() {
  const eventSource = new EventSource(
    'http://openhab:8080/rest/events?topics=smarthome/items/LIVING_ROOM_AMBILIGHT'
  );

  eventSource.addEventListener('close', () => process.exit(1));
  eventSource.addEventListener('error', () => process.exit(1));
  eventSource.addEventListener('open', () =>
    console.log('OpenHab connection established')
  );
  eventSource.addEventListener('message', (e) => {
    const data = JSON.parse(e.data);

    if (data.topic != 'smarthome/items/LIVING_ROOM_AMBILIGHT/state') {
      return;
    }

    const payload = JSON.parse(data.payload);

    if (payload.value == 'ON') {
      enableAmbilight().catch(console.log);
    } else {
      disableAmbilight().catch(console.log);
    }
  });
}
