import { disableAmbilight } from './ambilight';
import { establishOpenHabConnection } from './openhab';

disableAmbilight()
  .then(establishOpenHabConnection)
  .catch(() => process.exit(1));
