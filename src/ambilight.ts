import * as WebSocket from 'ws';

enum State {
  ON = 1,
  OFF = 0,
}

export const enableAmbilight = () => sendCommand(State.ON);
export const disableAmbilight = () => sendCommand(State.OFF);

async function sendCommand(state: State) {
  const instances = [0, 1];
  const ws = new WebSocket('ws://ambilight:8090');

  await new Promise((resolve, reject) => {
    ws.on('error', reject);
    ws.on('open', async () => {
      for (const instance of instances) {
        await new Promise((resolve) =>
          ws.send(
            JSON.stringify({
              command: 'instance',
              tan: 2,
              subcommand: 'switchTo',
              instance,
            }),
            resolve
          )
        );

        await new Promise((resolve) =>
          ws.send(
            JSON.stringify({
              command: 'componentstate',
              tan: 2,
              componentstate: { component: 'LEDDEVICE', state: !!state },
            }),
            resolve
          )
        );

        resolve();
      }

      ws.close();
      console.log('New Ambilight state: ' + state);
    });
  });
}
